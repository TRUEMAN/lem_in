/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 17:55:15 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/19 22:49:13 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** Create a new room and set all the variables of the new room to 0 or 'NULL'.
** If the room creation failled return 'NULL'.
*/

t_room	*create_room(void)
{
	t_room	*new_room;

	new_room = (t_room*)malloc(sizeof(t_room));
	if (new_room == NULL)
		return (NULL);
	new_room->linked_room = (t_room**)malloc(sizeof(t_room *));
	if (new_room->linked_room == NULL)
		return (NULL);
	new_room->linked_room[0] = NULL;
	new_room->name = NULL;
	new_room->nb_ants = 0;
	new_room->total_nb_ants = 0;
	new_room->ant_id = -1;
	new_room->room_type = 0;
	new_room->x = 0;
	new_room->y = 0;
	new_room->is_occupied = FREE;
	new_room->dst_from_end = 0;
	return (new_room);
}

/*
**	Remove a room and then return 'NULL'.
*/

t_room	*remove_room(t_room *room)
{
	int	i;

	if (room == NULL)
		return (NULL);
	if (room->name != NULL)
		free(room->name);
	room->name = NULL;
	if (room->linked_room != NULL)
	{
		i = -1;
		while (room->linked_room[++i] != NULL)
		{
			free(room->linked_room[i]);
			room->linked_room[i] = NULL;
		}
		free(room->linked_room);
		room->linked_room = NULL;
	}
	free(room);
	return (NULL);
}

/*
** Link the room a to the room b. If creating a link is impossible return (-2).
** If memory allocation failled return (-1).
*/

int		link_room_a_to_b(t_room *room_a, t_room *room_b)
{
	int		nb_link;
	t_room	**new_linked_room;

	if (room_a == NULL || room_b == NULL)
		return (-2);
	nb_link = 0;
	while (room_a->linked_room[nb_link] != NULL)
		++nb_link;
	new_linked_room = (t_room **)malloc(sizeof(t_room *) * (nb_link + 2));
	if (new_linked_room == NULL)
		return (-1);
	new_linked_room[nb_link + 1] = NULL;
	new_linked_room[nb_link] = room_b;
	nb_link = -1;
	while (room_a->linked_room[++nb_link] != NULL)
		new_linked_room[nb_link] = room_a->linked_room[nb_link];
	free(room_a->linked_room);
	room_a->linked_room = new_linked_room;
	return (0);
}

/*
** Remove a link betwen the room a and the room b. But not the connextion B->A.
*/

int		unlink_room_a_to_b(t_room *room_a, t_room *room_b)
{
	int		nb_link;
	int		link_to_room;
	t_room	**new_linked_room;

	if (room_a == NULL || room_b == NULL)
		return (-2);
	nb_link = 0;
	while (room_a->linked_room[nb_link] != NULL)
		++nb_link;
	new_linked_room = (t_room **)malloc(sizeof(t_room *) * nb_link);
	if (new_linked_room == NULL)
		return (-1);
	new_linked_room[nb_link - 1] = NULL;
	nb_link = -1;
	link_to_room = 0;
	while (room_a->linked_room[++nb_link] != NULL)
	{
		if (room_a->linked_room[nb_link] == room_b)
			link_to_room = 1;
		new_linked_room[nb_link] = room_a->linked_room[nb_link + link_to_room];
	}
	free(room_a->linked_room);
	room_a->linked_room = new_linked_room;
	return (link_to_room);
}

/*
** Find the adress of the room a and b via their names. Then link a to b and
** b to a. If one or both rooms are not found in the room list then nothing
** is done.
** na <=> name_a. nb<=>name_b
*/

void	link_room_by_name(t_room_lst *lst, const char *na, const char *nb)
{
	t_room_lst	*ptr;
	t_room		*room_a;
	t_room		*room_b;

	room_a = NULL;
	room_b = NULL;
	ptr = lst;
	while (ptr != NULL && room_a == NULL)
	{
		if (ft_strcmp(ptr->current_room->name, na) == 0)
			room_a = ptr->current_room;
		ptr = ptr->next_room;
	}
	ptr = lst;
	while (ptr != NULL && room_b == NULL)
	{
		if (ft_strcmp(ptr->current_room->name, nb) == 0)
			room_b = ptr->current_room;
		ptr = ptr->next_room;
	}
	if (room_a == NULL || room_b == NULL)
		return ;
	link_room_a_to_b(room_a, room_b);
	link_room_a_to_b(room_b, room_a);
}
