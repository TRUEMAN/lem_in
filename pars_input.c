/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_input.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 14:15:34 by vnoon             #+#    #+#             */
/*   Updated: 2016/08/19 16:12:25 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** Return the total number of ants.
** If the line is not entirely composed of digits then return -1.
*/

static int		get_total_ant(const char *line)
{
	int		nb_ants;

	if (ft_str_is_number(line))
		nb_ants = ft_atoi(line);
	else
		return (-1);
	return (nb_ants);
}

/*
** Analyse a room type line then create a new room add it to the list
** (create a new t_room_lst element and add it to the list if needed to store
** the new room).
** Then reset the type variable to REG_ROOM.
*/

static void		build_room(t_room_lst *ant_hive, const char *line, int *type)
{
	t_room_lst	*start;

	start = ant_hive;
	while (ant_hive->next_room != NULL)
		ant_hive = ant_hive->next_room;
	if (ant_hive->current_room == NULL)
	{
		ant_hive->current_room = create_room();
		ant_hive->current_room->total_nb_ants = ant_hive->total_nb_ants;
		extract_room_info(ant_hive->current_room, line, *type);
	}
	else if (ant_hive->next_room == NULL)
	{
		ant_hive = add_room_lst(ant_hive, create_room());
		ant_hive = ant_hive->next_room;
		ant_hive->current_room->total_nb_ants = ant_hive->total_nb_ants;
		extract_room_info(ant_hive->current_room, line, *type);
		ant_hive->next_room = NULL;
	}
	if (*type == START)
		ant_hive->current_room->nb_ants = ant_hive->total_nb_ants;
	if (*type == START)
		ant_hive->current_room->ant_id = 1;
	*type = REG_ROOM;
}

/*
** Analyse a link type line and and extract the name of rooms to link. Then it
** Send the name and room_list to the function link_room_by_name.
** If the names are valid them the room are linked. If not nothing is done.
*/

static void		link_room(t_room_lst *ant_hive, const char *line, int k)
{
	int		i;
	int		j;
	char	*room_a;
	char	*room_b;

	room_a = NULL;
	room_b = NULL;
	i = k;
	while (line[++i] != '\0' && room_a == NULL)
		if (is_there_such_room(line, ant_hive, i) == 1 && line[i] == '-')
			room_a = ft_strsub(line, 0, i);
	k = i;
	if (line[i] == '\0')
		return ;
	j = 0;
	while (line[i + j] != '\0')
		++j;
	if (is_there_such_room(line + i, ant_hive, j) == 1)
		room_b = ft_strsub(line, i, j);
	if (room_a == NULL || room_b == NULL || ft_strcmp(room_a, room_b) == 0)
		return (link_room(ant_hive, line, k));
	if (ft_strcmp(room_a, room_b) != 0)
		link_room_by_name(ant_hive, room_a, room_b);
}

/*
** Analyse if theline read then select the function ment to analyse it.
** If it's a ##START or ##END command line then the type variable is set to the
** appropriate value. So that the next call of the function build_room will
** create a room of the appropriate type.
*/

static void		pars_selector(t_room_lst *ant_hive, const char *line, int *type)
{
	int	selector;

	selector = is_line_valid(line, ant_hive);
	if (selector == COMMENT || selector == GEN_ERROR)
		return ;
	if (selector == START)
		*type = START;
	if (selector == END)
		*type = END;
	if (selector == ANT_LINE)
		ant_hive->total_nb_ants = get_total_ant(line);
	if (selector == ROOM_LINE)
		build_room(ant_hive, line, type);
	if (selector == ROOM_CONNECTION)
		link_room(ant_hive, line, 0);
}

t_room			*pars_input(int show_pars, t_room_lst **ant_hive)
{
	char		*line;
	char		*map;
	int			room_type;

	if ((*ant_hive = create_room_lst(NULL)) == NULL)
		return (NULL);
	if (get_next_line(0, &line) == 0 && ft_printf("ERROR\n"))
		return (NULL);
	if (line[0] == '\0' && ft_printf("ERROR\n"))
		return (NULL);
	room_type = REG_ROOM;
	map = NULL;
	while (is_line_valid(line, *ant_hive) != GEN_ERROR)
	{
		pars_selector(*ant_hive, line, &room_type);
		map = ft_str_safe_join(&line, &map);
		if (get_next_line(0, &line) <= 0)
			break ;
	}
	if (is_map_valid(*ant_hive) != 0)
		return (map_error_manager(is_map_valid(*ant_hive), show_pars));
	ft_putstr(map != NULL ? map : "ERROR\n");
	return ((*ant_hive)->current_room);
}
