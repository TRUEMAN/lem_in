/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_path.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/07 14:46:27 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/23 03:11:42 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** Renvoie le nombre de lien qu'a cette salle avec d'autres salles.
*/

static int		cpt_nb_link_of_room(const t_room *hive)
{
	int	max_link_start;

	max_link_start = 0;
	while (hive->linked_room[max_link_start] != NULL)
		++max_link_start;
	return (max_link_start);
}

static t_path	*path_adr_index(t_path *path, int i)
{
	t_path	*path_adr;

	path_adr = path;
	while (i != 0)
	{
		path_adr = path_adr->next_room;
		--i;
	}
	return (path_adr);
}

static void		rev_path(t_path *path)
{
	t_path	*ptr;
	int		len;
	int		i;

	len = 0;
	ptr = path;
	while (ptr != NULL)
	{
		++len;
		ptr = ptr->next_room;
	}
	i = 0;
	while (i != len / 2)
	{
		swap_path(path_adr_index(path, i), path_adr_index(path, len - i - 1));
		++i;
	}
}

t_path			**find_path(t_room *hive)
{
	t_path	**path;
	int		nb_max_path;
	int		i;

	nb_max_path = cpt_nb_link_of_room(hive);
	path = (t_path **)malloc(sizeof(t_path *) * nb_max_path + 1);
	path[nb_max_path] = NULL;
	while (--nb_max_path >= 0)
		path[nb_max_path] = create_path(hive);
	hive->is_occupied = OCCUPIED;
	build_path(&path);
	i = -1;
	while (path[++i] != NULL)
		rev_path(path[i]);
	return (path);
}
