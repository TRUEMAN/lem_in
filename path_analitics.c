/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_analitics.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 04:37:42 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/23 03:01:24 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		check_shrtst_path(t_path **all_path)
{
	int	i;

	i = -1;
	while (all_path[++i] != NULL)
	{
		if (all_path[i]->crt_room->room_type == START)
			if (all_path[i]->next_room != NULL)
				if (all_path[i]->next_room->crt_room->room_type == END)
					return (1);
	}
	return (0);
}

int		path_len(t_path *path)
{
	int len;

	len = 0;
	if (path == NULL)
		return (len);
	while (path != NULL)
	{
		++len;
		if (path->next_room == NULL)
			break ;
		path = path->next_room;
	}
	return (len);
}

void	set_shortest_path(t_path ***path)
{
	int		min_len;
	int		nb_path;
	int		i;
	t_path	**shortest_path;

	i = -1;
	min_len = -1;
	while (path[0][++i] != NULL)
		if (path_len(path[0][i]) < min_len || min_len == -1)
			min_len = path_len(path[0][i]);
	i = -1;
	nb_path = 0;
	while (path[0][++i] != NULL)
		if (path_len(path[0][i]) == min_len)
			++nb_path;
	if (!(shortest_path = (t_path **)malloc(sizeof(t_path *) * (nb_path + 1))))
		return ;
	shortest_path[nb_path] = NULL;
	i = -1;
	while (path[0][++i] != NULL && nb_path != 0)
		if (path_len(path[0][i]) == min_len)
			shortest_path[--nb_path] = path[0][i];
		else
			remove_all_room_to_path(path[i]);
	*path = shortest_path;
}

void	remove_path_too_long(t_path ***path)
{
	int		i;
	int		nb_good_path;
	t_path	**best_paths;

	i = -1;
	nb_good_path = 0;
	while (path[0][++i] != NULL)
		if (((*path[0])->crt_room->total_nb_ants) >= (path_len(path[0][i])))
			++nb_good_path;
	if (nb_good_path == i)
		return ;
	if (nb_good_path == 0)
		return (set_shortest_path(path));
	best_paths = (t_path **)malloc(sizeof(t_path*) * (nb_good_path + 1));
	if (best_paths == NULL)
		return ;
	best_paths[nb_good_path] = NULL;
	i = -1;
	while (*path[++i] != NULL && nb_good_path != 0)
		if (path[0][i]->crt_room->total_nb_ants >= path_len(path[0][i]))
			best_paths[--nb_good_path] = path[0][i];
		else
			remove_all_room_to_path(path[i]);
	*path = best_paths;
}
