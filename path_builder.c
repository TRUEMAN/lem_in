/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_builder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/11 12:39:46 by vnoon             #+#    #+#             */
/*   Updated: 2016/08/19 15:56:10 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	are_we_at_end(t_path *current_path)
{
	while (current_path != NULL && current_path->crt_room->room_type != END)
		current_path = current_path->next_room;
	if (current_path != NULL && current_path->crt_room->room_type == END)
		return (1);
	return (0);
}

static int	add_to_path(t_path *path, t_path **all_path)
{
	int	shortest_dst;
	int	j;
	int	i;

	if (are_we_at_end(path) == 1)
		return (-1);
	while (path != NULL && path->next_room != NULL)
		path = path->next_room;
	i = -1;
	shortest_dst = -1;
	j = -1;
	while (path->crt_room->linked_room[++i] != NULL)
		if ((shortest_dst > path->crt_room->linked_room[i]->dst_from_end ||
			shortest_dst == -1) &&
			(path->crt_room->linked_room[i]->is_occupied == FREE ||
			(path->crt_room->linked_room[i]->room_type == END &&
			(path->crt_room->room_type != START ||
			!check_shrtst_path(all_path)))))
		{
			shortest_dst = path->crt_room->linked_room[i]->dst_from_end;
			j = i;
		}
	if (j != -1)
		path->next_room = create_path(path->crt_room->linked_room[j]);
	return (j);
}

static int	are_path_done(t_path **path)
{
	int		i;
	int		nb_done_path;

	i = -1;
	nb_done_path = 0;
	while (path[++i] != NULL)
	{
		if (are_we_at_end(path[i]) == 1)
			++nb_done_path;
	}
	return (nb_done_path);
}

static void	remove_excess_path(t_path ***path)
{
	int		nb_valid_path;
	int		i;
	t_path	**valid_path;

	nb_valid_path = are_path_done(*path);
	valid_path = (t_path **)malloc(sizeof(t_path*) * (nb_valid_path + 1));
	if (valid_path == NULL)
		return ;
	valid_path[nb_valid_path] = NULL;
	i = -1;
	while (*path[++i] != NULL && nb_valid_path != 0)
	{
		if (are_we_at_end(path[0][i]) == 1)
			valid_path[--nb_valid_path] = path[0][i];
		else
			remove_all_room_to_path(path[i]);
	}
	*path = valid_path;
}

void		build_path(t_path ***path)
{
	int		i;
	int		nb_dead_end;
	t_path	**tempo;

	i = -1;
	nb_dead_end = 0;
	tempo = *path;
	while (are_path_done(tempo) != (i - nb_dead_end))
	{
		nb_dead_end = 0;
		i = -1;
		while (tempo[++i] != NULL)
			if (add_to_path(tempo[i], *path) == -1)
				if (are_we_at_end(tempo[i]) == 0)
					++nb_dead_end;
	}
	remove_excess_path(path);
	remove_path_too_long(path);
}
