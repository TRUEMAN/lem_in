/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room_lst_operations.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 17:25:33 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/24 06:52:07 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** Create a new room list. If a room is given as parameter then it also add the
** room addresse at the beginning of the room list.
*/

t_room_lst	*create_room_lst(t_room *room)
{
	t_room_lst	*new_lst;

	new_lst = (t_room_lst *)malloc(sizeof(t_room_lst));
	if (new_lst == NULL)
		return (NULL);
	new_lst->next_room = NULL;
	new_lst->current_room = room;
	new_lst->total_nb_ants = -1;
	return (new_lst);
}

/*
** Free a room list and the room linked to the room list then return 'NULL'.
*/

t_room_lst	*free_room_lst(t_room_lst *lst)
{
	t_room_lst	*lst_ptr;

	while (lst != NULL)
	{
		lst_ptr = lst;
		lst = lst->next_room;
		if (lst_ptr->current_room != NULL)
			remove_room(lst_ptr->current_room);
		free(lst_ptr);
	}
	return (NULL);
}

/*
** Add a new room to a room list. If a memory allocation occure then the list is
** destroyed and the memory freed.
*/

t_room_lst	*add_room_lst(t_room_lst *lst_room, t_room *new_room)
{
	t_room_lst	*lst_ptr;

	lst_ptr = lst_room;
	while (lst_ptr->next_room != NULL)
		lst_ptr = lst_ptr->next_room;
	lst_ptr->next_room = (t_room_lst *)malloc(sizeof(t_room_lst));
	if (lst_ptr->next_room == NULL)
		return (free_room_lst(lst_room));
	lst_ptr = lst_ptr->next_room;
	lst_ptr->next_room = NULL;
	lst_ptr->current_room = new_room;
	lst_ptr->total_nb_ants = lst_room->total_nb_ants;
	return (lst_room);
}

void		swap_room_lst(t_room_lst *room_a, t_room_lst *room_b)
{
	t_room	*ptr;

	ptr = room_a->current_room;
	room_a->current_room = room_b->current_room;
	room_b->current_room = ptr;
}
