/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_manager.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 06:39:13 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/29 14:12:50 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	is_it_a_flag(char c)
{
	if (c == 'a' || c == 'd' || c == 'h')
		return (1);
	return (0);
}

static int	is_it_valid(char *str)
{
	if (*str != '-' || *(str + 1) == '\0')
		return (0);
	++str;
	while (is_it_a_flag(*str) == 1)
		++str;
	if (*str != '\0')
		return (0);
	return (1);
}

static void	set_up_flags(t_options *options)
{
	options->advanced_error_flag = 0;
	options->solving_post_analysis = 0;
}

void		show_flags_usage(void)
{
	ft_putstr("usage: lem-in [-adh]\n\n");
	ft_putstr("a : Print details about parsing errors.\n");
	ft_putstr("d : Show some details about the solution found.");
	ft_putstr("Number of turn, number of move, path_found and used\n");
	ft_putstr("h : print this help message. Lem-in will not try to solve.\n");
}

int			get_flags(char *str, t_options *options)
{
	set_up_flags(options);
	if (str == NULL)
		return (1);
	else if (is_it_valid(str) == 0)
		return (-1);
	++str;
	while (is_it_a_flag(*str) == 1)
	{
		if (*str == 'a')
			options->advanced_error_flag = 1;
		if (*str == 'd')
			options->solving_post_analysis = 1;
		if (*str == 'h')
			return (0);
		++str;
	}
	return (1);
}
