/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 14:11:43 by vnoon             #+#    #+#             */
/*   Updated: 2016/08/19 16:11:55 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int	main(int argc, char **argv)
{
	t_room		*start;
	t_path		**path;
	t_room_lst	*ant_hive;
	t_options	options;
	int			flag_return;

	flag_return = get_flags(argc == 2 ? argv[1] : NULL, &options);
	if (flag_return == 0 || flag_return == -1 || argc > 2)
	{
		show_flags_usage();
		return (argc > 2 ? -1 : flag_return);
	}
	path = NULL;
	start = pars_input(options.advanced_error_flag, &ant_hive);
	if (start != NULL)
		path = find_path(start);
	if (path != NULL)
		solve(path, options.solving_post_analysis);
	return (0);
}
