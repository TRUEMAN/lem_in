/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hive_inspector.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 10:39:58 by vnoon             #+#    #+#             */
/*   Updated: 2016/12/07 22:34:19 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_room	*map_error_manager(int err_val, int adv_err_flag)
{
	ft_putstr("ERROR\n");
	if (adv_err_flag == 0)
		return (NULL);
	if ((err_val & 0x21) != 0)
		ft_putstr((err_val & 0x21) == 1 ? "No start.\n" : "Too many starts.\n");
	if ((err_val & 0x42) != 0)
		ft_putstr((err_val & 0x42) == 2 ? "No end.\n" : "Too many ends.\n");
	if ((err_val & 0x04) != 0)
		ft_putstr("No conexion between start and end.\n");
	if ((err_val & 0x08) != 0)
		ft_putstr("No number of ants specified, or an unvalid number.\n");
	if ((err_val & 0x10) != 0)
		ft_putstr("Some room(s) have the same name.\n");
	if ((err_val & 0x80) != 0)
		ft_putstr("The map has no rooms.\n");
	return (NULL);
}

char	*ft_str_safe_join(char **line, char **map)
{
	char	*new_map;

	if (*line == NULL)
		return (NULL);
	if (*map != NULL)
		new_map = ft_strnew(ft_strlen(*line) + ft_strlen(*map) + 1);
	else
		new_map = ft_strnew(ft_strlen(*line) + 1);
	if (new_map == NULL)
		return (NULL);
	if (*map != NULL)
		ft_strcpy(new_map, *map);
	if (*map != NULL)
		ft_strcpy((new_map + ft_strlen(*map)), *line);
	else
		ft_strcpy(new_map, *line);
	free(*map);
	free(*line);
	new_map[ft_strlen(new_map)] = '\n';
	return (new_map);
}
