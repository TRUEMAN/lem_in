/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 18:46:18 by vnoon             #+#    #+#             */
/*   Updated: 2016/01/27 15:35:27 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 5
# include <stdlib.h>
# include <unistd.h>

typedef struct				s_maillon
{
	int					fd;
	char				*str;
	struct s_maillon	*prv;
	struct s_maillon	*nxt;
}							t_maillon;

int							get_next_line(const int fd, char **line);

#endif
