/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 14:31:29 by vnoon             #+#    #+#             */
/*   Updated: 2015/12/15 11:34:25 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	char	*source;
	size_t	size_dst;
	size_t	size_src;
	int		len;

	size_dst = ft_strlen((const char*)dst);
	size_src = ft_strlen(src);
	if (size <= size_dst)
		return (size + size_src);
	source = (char *)src;
	len = (int)(size - 1 - size_dst);
	dst = dst + size_dst;
	while (*source && len-- > 0)
		*dst++ = *source++;
	*dst = 0;
	return (size_dst + size_src);
}
