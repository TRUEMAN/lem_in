/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 18:44:21 by vnoon             #+#    #+#             */
/*   Updated: 2016/01/27 14:56:40 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "./libft/libft.h"

int		secur_join(char **str_fd, char *buff)
{
	char		*ptr;

	ptr = *str_fd;
	if ((*str_fd = ft_strjoin(*str_fd, buff)) == NULL)
	{
		free(ptr);
		return (-1);
	}
	free(ptr);
	return (1);
}

int		clean(int i, const int fd, char **str, char **line)
{
	char		*ptr;

	if ((*line = ft_strnew(i)) == NULL)
		return (-1);
	ft_strncpy(*line, str[fd], i);
	if (str[fd][i + 1] == '\0')
	{
		free(str[fd]);
		str[fd] = NULL;
	}
	else
	{
		ptr = str[fd];
		if ((str[fd] = ft_strsub(str[fd], i + 1, ft_strlen(str[fd]))) == NULL)
		{
			free(ptr);
			return (-1);
		}
		free(ptr);
	}
	return (1);
}

int		get_next_line(int const fd, char **line)
{
	static char	*str[256];
	char		buff[BUFF_SIZE + 1];
	int			i;

	if ((fd > 255) || (fd < 0) || (BUFF_SIZE < 1) || (line == NULL))
		return (-1);
	while ((i = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[i] = '\0';
		if (str[fd] == NULL)
		{
			if ((str[fd] = ft_strdup(buff)) == NULL)
				return (-1);
		}
		else if (secur_join(&str[fd], buff) == -1)
			return (-1);
		if (ft_strchr(str[fd], '\n') != NULL)
			break ;
	}
	if ((str[fd] == NULL) || (i < 0))
		return (i);
	i = 0;
	while ((str[fd][i] != '\0') && (str[fd][i] != '\n'))
		++i;
	return (clean(i, fd, str, line));
}
