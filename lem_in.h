/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 14:03:21 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/29 14:20:29 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# define GEN_ERROR -42
# define START 0
# define REG_ROOM 1
# define END 2
# define ANT_LINE 3
# define ROOM_LINE 4
# define ROOM_CONNECTION 5
# define COMMENT 6

# define OCCUPIED 10
# define FREE 11
# define DEADEND 12

# define DEBUG ft_printf("Here:%d\n", __LINE__);

# include "libft/libft.h"
# include "libftprintf/ft_printf.h"
# include "gnl/get_next_line.h"

typedef struct	s_room
{
	char			*name;
	int				nb_ants;
	int				total_nb_ants;
	int				ant_id;
	int				room_type;
	int				x;
	int				y;
	int				dst_from_end;
	char			is_occupied;
	struct s_room	**linked_room;
}				t_room;

typedef struct	s_room_lst
{
	int					total_nb_ants;
	t_room				*current_room;
	struct s_room_lst	*next_room;
}				t_room_lst;

typedef struct	s_path
{
	t_room			*crt_room;
	struct s_path	*next_room;
}				t_path;

typedef struct	s_options
{
	int		advanced_error_flag;
	int		solving_post_analysis;
}				t_options;

t_room			*pars_input(int show_pars, t_room_lst **ant_hive);

t_room_lst		*create_room_lst(t_room *room);
t_room_lst		*free_room_lst(t_room_lst *room);
t_room_lst		*add_room_lst(t_room_lst *lst_room, t_room *new_room);
void			swap_room_lst(t_room_lst *room_a, t_room_lst *room_b);

t_room			*create_room(void);
t_room			*remove_room(t_room *room);
int				link_room_a_to_b(t_room *room_a, t_room *room_b);
void			link_room_by_name(t_room_lst *ls, const char *a, const char *b);

int				is_there_such_room(const char *l, const t_room_lst *str, int i);
int				is_line_valid(const char *line, const t_room_lst *start);

void			extract_room_info(t_room *room, const char *line, int type);

void			show_post_parsing(t_room_lst *ant_hive);
t_room			*map_error_manager(int err_val, int adv_err_flag);
char			*ft_str_safe_join(char **line, char **map);

int				is_map_valid(t_room_lst *lst);

t_path			*create_path(t_room *start);
int				add_room_to_path(t_path *path_start, t_room *new_room);
int				remove_last_room_to_path(t_path **path_start);
int				remove_all_room_to_path(t_path **path_start);
void			swap_path(t_path *a, t_path *b);

void			build_path(t_path ***path);
t_path			**find_path(t_room *hive);

void			solve(t_path **path, int show_details);
int				check_shrtst_path(t_path **all_path);
int				path_len(t_path *path);
void			remove_path_too_long(t_path ***path);

int				get_flags(char *str, t_options *options);
void			show_flags_usage(void);

#endif
