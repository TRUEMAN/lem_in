/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_validity.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/01 10:59:55 by vnoon             #+#    #+#             */
/*   Updated: 2016/08/04 04:56:37 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** Compte le nombre de start et de end existant apres parsing.
** Si occun probleme renvoie 0.
** Si trop ou pas assez de start return 1.
** Si trop ou pas assez de end return 2.
** Si trop ou pas assez de start & de end return 3.
*/

static int	check_start_end(t_room_lst *lst)
{
	int	nb_start;
	int	nb_end;
	int	validity_index;

	nb_start = 0;
	nb_end = 0;
	while (lst != NULL)
	{
		nb_start += (lst->current_room->room_type == 0) ? 1 : 0;
		nb_end += (lst->current_room->room_type == 2) ? 1 : 0;
		lst = lst->next_room;
	}
	validity_index = 0;
	if (nb_start != 1)
		validity_index += nb_start > 1 ? 32 : 1;
	if (nb_end != 1)
		validity_index += nb_end > 1 ? 64 : 2;
	return (validity_index);
}

/*
** calcule la distance des salles adjacentes par rapport a la end si la distance
** par rapport a la end si la salle donnee en paramettre est a une distance
** connue de la end.
*/

static void	edit_start_dist(t_room *room)
{
	int	i;

	i = -1;
	while (room->linked_room[++i] != NULL)
	{
		if (room->linked_room[i]->room_type != END &&
			room->linked_room[i]->dst_from_end == 0)
			room->linked_room[i]->dst_from_end = room->dst_from_end + 1;
	}
}

/*
** Retourn la distance entre la end et la salle la plus loin de la end.
*/

static int	get_biggest_dist(t_room_lst *lst)
{
	int	dst;

	dst = lst->current_room->dst_from_end;
	while (lst != NULL)
	{
		if (dst < lst->current_room->dst_from_end)
			dst = lst->current_room->dst_from_end;
		lst = lst->next_room;
	}
	return (dst);
}

/*
** Place la start room dans le premier maillon  de la list chainee hive.
** Puis determine la distance de chaque piece par rapport a end room et stock
** cette information dans la structure definisant chaque salle.
*/

static void	set_up_start_and_dist(t_room_lst *hive, int i)
{
	t_room_lst	*ptr;

	ptr = hive;
	while (ptr->current_room->room_type != START)
		ptr = ptr->next_room;
	swap_room_lst(ptr, hive);
	ptr = hive;
	while (ptr->current_room->room_type != END)
		ptr = ptr->next_room;
	edit_start_dist(ptr->current_room);
	ptr = hive;
	i = 1;
	while (ptr != NULL)
	{
		if (ptr->current_room->dst_from_end == i)
			edit_start_dist(ptr->current_room);
		ptr = ptr->next_room;
		if (ptr == NULL)
		{
			if (get_biggest_dist(hive) != (++i))
				break ;
			else
				ptr = hive->next_room;
		}
	}
}

/*
** Renvoie 0 si la carte est valie si elle est invalide renvoie une valeur
** entre 1 & 7.
** Annalyse des erreurs
** Si le bit de poid 4 est a 1: Pas de connexion entre start & end.
** Si le bit de poid 2 est a 1: Pas assez ou trop de salles de type END.
** Si le bit de poid 1 est a 1: Pas assez ou trop de salles de type START.
*/

int			is_map_valid(t_room_lst *lst)
{
	int			validity;
	int			i;
	t_room_lst	*pt;

	validity = (lst->current_room != NULL) ? check_start_end(lst) : 128;
	if (validity == 0)
	{
		set_up_start_and_dist(lst, 0);
		pt = lst;
		while (pt->current_room->room_type != START)
			pt = pt->next_room;
		validity += (pt->current_room->dst_from_end == 0) ? 4 : 0;
	}
	validity += (lst->total_nb_ants < 0) ? 8 : 0;
	pt = lst;
	while (pt != NULL && (validity & 0x80) == 0)
	{
		i = ft_strlen(pt->current_room->name);
		if ((is_there_such_room(pt->current_room->name, lst, i) == 1) &&
			pt->next_room != NULL &&
			(is_there_such_room(pt->current_room->name, pt->next_room, i) == 1))
			validity += (validity & 0x10) == 0 ? 16 : 0;
		pt = pt->next_room;
	}
	return (validity);
}
