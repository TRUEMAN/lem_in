# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vnoon <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/28 16:04:25 by vnoon             #+#    #+#              #
#    Updated: 2016/06/24 06:02:07 by vnoon            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=lem-in
SRC=lem_in.c pars_input.c line_analitics.c room_operations.c \
room_lst_operations.c extract_line_data.c hive_inspector.c check_map_validity.c\
path_builder.c find_path.c path_operations.c solve.c path_analitics.c \
flag_manager.c
OBJ=$(SRC:.c=.o)
LIB=libft/
LIB_NAME=libft.a
LIB_PRINT=libftprintf/
LIB_PRINT_NAME=libftprintf.a
LIB_GNL=gnl/
LIB_GNL_NAME=get_next_line.a
FLAG=-Wall -Wextra -Werror

$(NAME): $(OBJ)
	gcc -o $(NAME) $(OBJ) $(LIB)$(LIB_NAME) $(LIB_PRINT)$(LIB_PRINT_NAME) \
	$(LIB_GNL)$(LIB_GNL_NAME)

all:$(NAME)

clean:
	make -C $(LIB) clean
	make -C $(LIB_PRINT) clean
	make -C $(LIB_GNL) clean
	rm -f $(OBJ)

fclean:clean
	make -C $(LIB) fclean
	make -C $(LIB_PRINT) fclean
	make -C $(LIB_GNL) fclean
	rm -f $(NAME)

re:fclean all

mclean:all clean

%.o:%.c
	make -C $(LIB)
	make -C $(LIB_PRINT)
	make -C $(LIB_GNL)
	gcc -c $(FLAG) $(SRC)
