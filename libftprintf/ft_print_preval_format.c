/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_preval_format.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 10:56:40 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:34:30 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_print_sharp_format(uintmax_t val, t_data dt)
{
	if ((dt.format_flag & sharp_flag) == sharp_flag)
	{
		if (((dt.var_id == 'x' || dt.var_id == 'X') &&
				(val != 0)) || dt.var_id == 'p')
		{
			ft_putchar('0');
			ft_putchar(dt.var_id == 'p' ? 'x' : dt.var_id);
			return (2);
		}
		if ((dt.var_id == 'o' || dt.var_id == 'O') &&
				((val != 0) || (dt.precision != 0)))
		{
			ft_putchar('0');
			return (1);
		}
	}
	return (0);
}

static int	ft_print_space_plus_flag(char is_sgnd, t_data dt)
{
	int	nb_printed;

	nb_printed = 0;
	if (dt.var_id == 'u' || dt.var_id == 'U' || dt.var_id == 'o' ||
		dt.var_id == 'O' || dt.var_id == 'x' || dt.var_id == 'X')
		return (nb_printed);
	if ((dt.format_flag & plus_flag))
	{
		ft_putchar(is_sgnd != 3 ? '+' : '-');
		++nb_printed;
	}
	else if ((dt.format_flag & space_flag) && (is_sgnd != 3))
	{
		ft_putchar(' ');
		++nb_printed;
	}
	else if ((is_sgnd) == 3)
	{
		ft_putchar('-');
		++nb_printed;
	}
	return (nb_printed);
}

int			ft_print_padding(uintmax_t val, char is_sgnd, t_data dt)
{
	int		to_pad;
	int		num_size;
	int		nb_printed;

	num_size = ft_num_length(val, is_sgnd, dt);
	nb_printed = 0;
	to_pad = dt.min_size;
	to_pad -= num_size > dt.precision ? num_size : dt.precision + (is_sgnd / 3);
	to_pad -= (dt.format_flag & space_flag) ? 1 : 0;
	to_pad -= (dt.format_flag & plus_flag) && is_sgnd != 3 ? 1 : 0;
	while (--to_pad >= 0)
	{
		ft_putchar(' ');
		++nb_printed;
	}
	if (dt.precision < 0 && val == 0 && dt.min_size != 0 &&
		(dt.var_id == 'x' || dt.var_id == 'X' || dt.var_id == 'o' ||
		dt.var_id == 'O' || dt.var_id == 'd' || dt.var_id == 'O'))
	{
		ft_putchar(' ');
		++nb_printed;
	}
	return (nb_printed);
}

int			ft_print_precision(uintmax_t val, t_data dt, char is_sgnd)
{
	int		nb_printed;
	int		num_size;
	int		to_print;

	if (dt.format_flag & zero_flag)
		dt.precision = dt.min_size;
	num_size = ft_num_length(val, is_sgnd, dt);
	to_print = dt.precision - num_size;
	if (((dt.format_flag & plus_flag) && (((dt.format_flag & zero_flag) == 0) &&
			is_sgnd != 3)) || (((dt.format_flag & zero_flag) == 0) &&
			(is_sgnd == 3) && dt.precision > 0))
		++to_print;
	nb_printed = 0;
	if ((dt.var_id == 'd' || dt.var_id == 'D') &&
			(dt.format_flag & zero_flag) && (dt.format_flag & space_flag))
		to_print -= 1;
	if ((dt.format_flag & sharp_flag) && (val != 0 || dt.precision != 0) &&
		((dt.format_flag & zero_flag) == 0))
		to_print += (dt.var_id == 'o' || dt.var_id == 'O') ? 0 : 2;
	while (--to_print >= 0)
	{
		++nb_printed;
		ft_putchar('0');
	}
	return (nb_printed);
}

int			ft_print_prvl_frmt(uintmax_t val, char is_sgnd, t_data dt)
{
	int	nb_printed;

	nb_printed = 0;
	if ((dt.format_flag & minus_flag) || dt.precision != 0)
		dt.format_flag -= (dt.format_flag & zero_flag) ? zero_flag : 0;
	if (((dt.format_flag & minus_flag) != minus_flag) &&
			((dt.format_flag & zero_flag) != zero_flag))
		nb_printed += ft_print_padding(val, is_sgnd, dt);
	nb_printed += ft_print_space_plus_flag(is_sgnd, dt);
	nb_printed += ft_print_sharp_format(val, dt);
	nb_printed += ft_print_precision(val, dt, is_sgnd);
	return (nb_printed);
}
