/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_c.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 21:37:46 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:34:00 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <unistd.h>

static	int		print(unsigned short c)
{
	write(1, (void *)&c, 1);
	return (1);
}

static	wchar_t	ft_manage_pre_format(va_list *prm, t_data dt, int *len)
{
	wchar_t	arg;

	arg = 0;
	if (dt.size_flag[0] == 'l')
		arg = ((wchar_t)va_arg(*prm, wchar_t));
	else
		arg = ((char)va_arg(*prm, wchar_t));
	if (arg > 255)
		return (arg);
	while (((dt.min_size - *len) > 1) && ((dt.format_flag & minus_flag) == 0))
	{
		ft_putchar(dt.format_flag & zero_flag ? '0' : ' ');
		++*len;
	}
	return (arg);
}

static	void	ft_manage_post_format(t_data dt, int *len)
{
	while (((dt.min_size - *len) > 0) && ((dt.format_flag & minus_flag) != 0))
	{
		ft_putchar(dt.format_flag & zero_flag ? '0' : ' ');
		++*len;
	}
}

int				ft_print_c(va_list *prm, t_data dt)
{
	int					mask;
	wchar_t				pen;
	int					ln;

	ln = 0;
	pen = ft_manage_pre_format(prm, dt, &ln);
	if (pen > 127)
	{
		if (pen >= 65536)
			ln += print((0xF0 | ((pen >> 15) & 0x07)));
		mask = (pen < 65536) ? 0x0F : 0x3F;
		if (pen >= 2048)
			ln += print(((pen < 65536 ? 0xE0 : 0x80) | ((pen >> 12) & mask)));
		mask = (pen < 127) ? 0x1F : 0x3F;
		ln += print(((pen < 2048) ? 0xC0 : 0x80) | ((pen >> 6) & mask));
		ln += print((0x80 | (pen & 0x3F)));
	}
	else
		ln += print(pen);
	ft_manage_post_format(dt, &ln);
	return (ln);
}

int				ft_print_sc(const void *prm, t_data dt)
{
	const unsigned int	*wpn;
	const char			*pn;
	int					mask;
	int					nc;

	wpn = prm;
	pn = prm;
	nc = 0;
	if (dt.size_flag[0] == 'l' && (*wpn > 127))
	{
		if (*wpn >= 65536)
			nc += print((0xF0 | ((*wpn >> 15) & 0x07)));
		mask = (*wpn < 65536) ? 0x0F : 0x3F;
		if (*wpn >= 2048)
			nc += print(((*wpn < 65536 ? 0xE0 : 0x80) | ((*wpn >> 12) & mask)));
		mask = (*wpn < 127) ? 0x1F : 0x3F;
		nc += print(((*wpn < 2048) ? 0xC0 : 0x80) | ((*wpn >> 6) & mask));
		nc += print((0x80 | (*wpn & 0x3F)));
	}
	else if (dt.size_flag[0] == 'l')
		nc += print(*wpn);
	else
		nc += print(*pn);
	return (nc);
}
