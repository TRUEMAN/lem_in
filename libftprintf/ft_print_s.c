/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_s.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 20:24:05 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:35:07 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_get_char_size(wchar_t car)
{
	if (car >= 65536)
		return (4);
	if (car >= 2048)
		return (3);
	if (car >= 128)
		return (2);
	return (1);
}

static int	ft_gln(const wchar_t *s1, const char *s2, t_data dt)
{
	int	len;

	len = 0;
	if (dt.size_flag[0] == 'l')
		while ((*s1 != 0) && (((ft_get_char_size(*s1) + len) <= dt.precision) ||
			(dt.precision == 0)))
		{
			len += ft_get_char_size(*s1);
			++s1;
		}
	else
		while (*s2 != '\0')
		{
			++s2;
			++len;
		}
	if (dt.precision != 0)
		len = len > dt.precision ? dt.precision : len;
	if (len == -1)
		len = 0;
	return (len);
}

static int	ft_putncarac(int *nb, const wchar_t *s1, const char *s2, t_data dt)
{
	int		nb_print;
	int		byte_printed;

	nb_print = 0;
	byte_printed = 0;
	if (dt.size_flag[0] == 'l')
		while (*nb >= byte_printed + ft_get_char_size(s1[nb_print]))
		{
			byte_printed += ft_print_sc(&s1[nb_print], dt);
			++nb_print;
		}
	else
		while (*nb > byte_printed)
		{
			byte_printed += ft_print_sc(&s2[nb_print], dt);
			++nb_print;
		}
	*nb = byte_printed;
	return (byte_printed);
}

int			ft_getvar(wchar_t **wpen, char **pen, t_data dt, va_list *prm)
{
	*wpen = NULL;
	*pen = NULL;
	if (dt.size_flag[0] == 'l')
		*wpen = va_arg(*prm, wchar_t *);
	else
		*pen = va_arg(*prm, char *);
	if (*pen == NULL)
		*pen = "(null)";
	if (*wpen == NULL)
		*wpen = L"(null)";
	return (0);
}

int			ft_print_s(va_list *prm, t_data dt)
{
	int				index;
	char			*pen;
	wchar_t			*wpen;
	int				len;

	if (ft_getvar(&wpen, &pen, dt, prm) == -1)
		return (-1);
	len = ft_gln(wpen, pen, dt);
	index = 0;
	if ((dt.format_flag & minus_flag) == minus_flag)
	{
		index = ft_putncarac(&len, wpen, pen, dt) - 1;
		while (++index < dt.min_size)
			ft_putchar(dt.format_flag & zero_flag ? '0' : ' ');
	}
	else
	{
		while ((index + len) < dt.min_size)
		{
			ft_putchar(dt.format_flag & zero_flag ? '0' : ' ');
			++index;
		}
		index += ft_putncarac(&len, wpen, pen, dt);
	}
	return (index);
}
