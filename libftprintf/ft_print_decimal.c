/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_decimal.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 12:47:50 by vnoon             #+#    #+#             */
/*   Updated: 2016/03/16 13:34:19 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_print_signed_base(uintmax_t val, t_data dt)
{
	int	nb_printed;
	int	base;

	val = ft_num_var_formater(val, dt);
	if ((dt.var_id == 'o') || (dt.var_id == 'O'))
		base = 8;
	else if ((dt.var_id == 'x') || (dt.var_id == 'X') || (dt.var_id == 'p'))
		base = 16;
	else
		base = 10;
	if (dt.var_id == 'p')
		dt.var_id = 'x';
	nb_printed = 0;
	if (val >= (uintmax_t)base)
	{
		nb_printed += ft_print_signed_base(val / base, dt);
		nb_printed += ft_print_signed_base(val % base, dt);
	}
	else
	{
		ft_putchar(val >= 10 ? dt.var_id - ('x' - 'a') + val % 10 : '0' + val);
		return (1);
	}
	return (nb_printed);
}

int			ft_prnt_dcml(uintmax_t uval, intmax_t val, t_data dt, char sgn)
{
	int				char_printed;

	if (sgn)
	{
		char_printed = ft_print_prvl_frmt(val < 0 ? -val : val, sgn, dt);
		if (val != 0 || dt.precision != -1)
			char_printed += ft_print_signed_base(val < 0 ? -val : val, dt);
	}
	else
	{
		char_printed = ft_print_prvl_frmt(uval, sgn, dt);
		if (uval != 0 || dt.precision != -1)
			char_printed += ft_print_signed_base(uval, dt);
	}
	if ((dt.format_flag & minus_flag))
	{
		if (sgn)
			char_printed += ft_print_padding(sgn == 3 ? -val : val, sgn, dt);
		else
			char_printed += ft_print_padding(uval, sgn, dt);
	}
	return (char_printed);
}
