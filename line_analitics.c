/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_analitics.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/19 18:49:15 by vnoon             #+#    #+#             */
/*   Updated: 2016/08/19 16:11:38 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	is_room_position(const char *line)
{
	int	i;

	i = 0;
	if (line[0] == '#' || line[0] == 'L')
		return (-1);
	while (line[i] != '\0' && line[i] != ' ')
		++i;
	if (line[i] == '\0')
		return (-1);
	++i;
	while (ft_isdigit(line[i]))
		++i;
	if (line[i] != ' ')
		return (-1);
	++i;
	while (ft_isdigit(line[i]))
		++i;
	if (line[i] != '\0')
		return (-1);
	return (1);
}

static int	free_and_return(char *cpy, int to_return)
{
	free(cpy);
	return (to_return);
}

int			is_there_such_room(const char *line, const t_room_lst *start, int i)
{
	char				*cpy;
	const t_room_lst	*ptr;

	cpy = ft_strnew(i);
	if (cpy == NULL)
		return (-2);
	ft_strncpy(cpy, line, i);
	ptr = start;
	while (ptr != NULL)
	{
		if (ptr->current_room->name == NULL)
			return (free_and_return(cpy, -2));
		if (ft_strcmp(ptr->current_room->name, cpy) == 0)
			return (free_and_return(cpy, 1));
		if (ptr->next_room != NULL)
			ptr = ptr->next_room;
		else
			break ;
	}
	if (ptr->current_room->name == NULL)
		return (free_and_return(cpy, -2));
	if (ft_strcmp(ptr->current_room->name, cpy) == 0)
		return (free_and_return(cpy, 1));
	return (free_and_return(cpy, 0));
}

static int	is_room_connection(const char *line, const t_room_lst *start)
{
	int	i;
	int	j;
	int	nb_room;

	if (start == NULL || start->current_room == NULL)
		return (-1);
	nb_room = 0;
	i = 0;
	j = 0;
	while (line[i] != '\0' && line[i] != ' ')
	{
		if (line[i] == '-')
			if (is_there_such_room(line + j, start, i) == 1)
			{
				nb_room++;
				j = i;
			}
		if (nb_room > 2)
			return (-1);
		++i;
	}
	if (line[i] == ' ')
		return (-1);
	return (1);
}

int			is_line_valid(const char *line, const t_room_lst *start)
{
	char		over_flow;
	static int	part;

	if (line[0] == '\0')
		return (GEN_ERROR);
	if (ft_str_is_number(line) && ft_sec_atoi(line, &over_flow))
		return ((over_flow == 1) && (part == 0) ? GEN_ERROR : ANT_LINE);
	part = part == 0 ? 1 : part;
	if (ft_strcmp(line, "##start") == 0)
		return (part == 1 ? START : GEN_ERROR);
	if (ft_strcmp(line, "##end") == 0)
		return (part == 1 ? END : GEN_ERROR);
	if (line[0] == '#')
		return (COMMENT);
	over_flow = 0;
	if (is_room_position(line) == 1)
		return (part == 1 ? ROOM_LINE : GEN_ERROR);
	part = part > 1 ? part : 2;
	if (is_room_connection(line, start) == 1)
		return (part == 2 ? ROOM_CONNECTION : GEN_ERROR);
	return (GEN_ERROR);
}
