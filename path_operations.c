/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/07 15:47:07 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/23 02:50:17 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

/*
** Create a instance of path and set the current_room of the new path to the
** the adress of the room given to the function. Return NULL in case of
** miss-allocation.
*/

t_path	*create_path(t_room *start)
{
	t_path	*new_path;

	new_path = (t_path *)malloc(sizeof(t_path));
	if (new_path == NULL)
		return (NULL);
	new_path->crt_room = start;
	new_path->next_room = NULL;
	start->is_occupied = OCCUPIED;
	return (new_path);
}

/*
** Add an element to the end of t_path list. If a room is specified then
** the specified room adress is memorised in the new t_path element.
** If no problem occured return 0.
** If the new t_path structure cannot be allocated return -1.
** If the path_start equal NULL, return -2.
*/

int		add_room_to_path(t_path *path_start, t_room *new_room)
{
	t_path	*ptr;

	if (path_start == NULL)
		return (-2);
	ptr = path_start;
	while (ptr->next_room != NULL)
		ptr = ptr->next_room;
	ptr->next_room = create_path(new_room);
	if (ptr->next_room == NULL)
		return (-1);
	return (0);
}

/*
** Free the last room on a t_path list, then if the last room was remove set
** path_start to NULL.
** If it was not the last room set the last room of the list, the t_path element
** before the removed element now poit to NULL instead of the removed t_path
** element.
** If path_start equal NULL, return -2.
** If *path_start equal NULL, return -1.
** If it was the last element being removed, return 1.
** If the element removed wasn't the last element the list return 0.
*/

int		remove_last_room_to_path(t_path **path_start)
{
	t_path	*ptr;

	if (path_start == NULL)
		return (-2);
	if (*path_start == NULL)
		return (-1);
	ptr = *path_start;
	while (ptr->next_room != NULL && ptr->next_room->next_room != NULL)
		ptr = ptr->next_room;
	ptr->crt_room->is_occupied = FREE;
	if (ptr == *path_start && ptr->next_room == NULL)
	{
		free(ptr);
		*path_start = NULL;
		return (1);
	}
	free(ptr->next_room);
	ptr->next_room = NULL;
	return (0);
}

/*
** Remove all element from the t_path list included the first element.
** If path_start equal NULL, return -2.
** If *path_start equal NULL, return -1.
** If no problem occured, all room a freed and path_start equal NULL,
** then return 0.
*/

int		remove_all_room_to_path(t_path **path_start)
{
	if (path_start == NULL)
		return (-2);
	if (*path_start == NULL)
		return (-1);
	while (*path_start != NULL)
		remove_last_room_to_path(path_start);
	return (0);
}

void	swap_path(t_path *a, t_path *b)
{
	t_room	*tempo;

	tempo = a->crt_room;
	a->crt_room = b->crt_room;
	b->crt_room = tempo;
}
