/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/19 04:05:09 by vnoon             #+#    #+#             */
/*   Updated: 2016/06/29 14:53:15 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	update_ant_position(t_path *path)
{
	t_path	*ptr;
	int		nb_move;

	ptr = path;
	nb_move = 0;
	while (ptr->crt_room->room_type != START)
	{
		if (((ptr->crt_room->ant_id == -1) || (ptr->crt_room->room_type == END))
			&& ptr->next_room->crt_room->nb_ants > 0 &&
			ptr->next_room->crt_room->ant_id != -1)
		{
			ptr->crt_room->ant_id = ptr->next_room->crt_room->ant_id;
			ptr->next_room->crt_room->ant_id = -1;
			ptr->next_room->crt_room->nb_ants--;
			ptr->crt_room->nb_ants++;
			++nb_move;
			ft_printf("L%d-%s ", ptr->crt_room->ant_id, ptr->crt_room->name);
		}
		ptr = ptr->next_room;
	}
	ptr->crt_room->ant_id = ptr->crt_room->total_nb_ants + 1;
	ptr->crt_room->ant_id -= ptr->crt_room->nb_ants;
	return (nb_move);
}

void		show_detailed_info(int nb_move, int nb_turn, t_path **path)
{
	int		nb_path;
	t_path	*ptr;

	nb_path = 0;
	while (path[nb_path] != NULL)
		++nb_path;
	ft_printf("\nSolved in %d turns, in %d moves.\n", nb_turn, nb_move);
	ft_printf("%d path(s) were used.\n", nb_path);
	nb_path = 0;
	while (path[nb_path] != NULL)
	{
		ptr = path[nb_path];
		ft_printf("Path number %d:", nb_path + 1);
		while (ptr->crt_room->room_type != START)
		{
			ft_printf("[%s]<-", ptr->crt_room->name);
			ptr = ptr->next_room;
		}
		ft_printf("[%s]\n", ptr->crt_room->name);
		++nb_path;
	}
}

void		solve(t_path **path, int show_details)
{
	int	nb_move;
	int	nb_turn;
	int	i;

	nb_move = 0;
	nb_turn = 0;
	ft_putchar('\n');
	while (path[0]->crt_room->nb_ants / path[0]->crt_room->total_nb_ants == 0)
	{
		i = -1;
		while (path[++i] != NULL)
			nb_move += update_ant_position(path[i]);
		++nb_turn;
		ft_putchar('\n');
	}
	if (show_details == 1)
		show_detailed_info(nb_move, nb_turn, path);
}
