Pour rappel le lem_in est seulement sensé lire dans l'entrée standard, pas dans les fichiers.

Ce lem_in dispose de quelques flag bonus.


lem_in git:(master) ✗ ./lem-in -h
usage: lem-in [-adh]

a : Print details about parsing errors.
d : Show some details about the solution found.Number of turn, number of move, path_found and used
h : print this help message. Lem-in will not try to solve.