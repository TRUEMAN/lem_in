/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extract_line_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vnoon <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/26 19:15:13 by vnoon             #+#    #+#             */
/*   Updated: 2016/05/28 16:42:32 by vnoon            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	extract_room_info(t_room *room, const char *line, int type)
{
	int	i;

	i = 0;
	while (line[i] != ' ')
		++i;
	room->name = ft_strnew(i);
	if (room->name == NULL)
		return ;
	ft_strncpy(room->name, line, i);
	room->x = ft_atoi(line + (i + 1));
	++i;
	while (line[i] != ' ')
		++i;
	room->y = ft_atoi(line + (i + 1));
	room->room_type = type;
}
